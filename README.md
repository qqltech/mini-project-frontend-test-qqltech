# Mini Project Frontend Test QQLTECH

Testing Perekrutan Programmer Frontend PT Quantum Leap

## Project Hints:
1. Pelajari VUE JS Versi 2, link: https://vuejs.org/
2. Pelajari Vue Router, link : https://router.vuejs.org/
3. Pelajari Vuex, Link: https://vuex.vuejs.org/
4. Pelajari Restful Api Request
5. Pelajari Penggunaan POSTMAN, link: https://www.postman.com/
6. Pelajari Tema Komponen (salah satunya bootstrap, link: https://bootstrap-vue.org/)

## Task Vue Project dengan Tema Komponen Bootstrap-vue:
1. Buat Halaman Login berisi username dan password, dengan 1 Button Login
2. Buat Halaman utama berisi Kalimat Hello World
3. Buat middleware router agar user yang belum login tidak bisa mengakses halaman utama dan otomatis ter-redirect ke halaman login
4. Lakukan login ke API [method:POST]:
    endpoint URL: ```https://server7.riset.my.id/login``` menggunakan FETCH API
    dengan payload JSON
    ```
    {
        "username": "passwordinput",
        "password": "passwordinput"
    }
    ```
5. Validasi agar username dan password terisi (tidak boleh kosong) sebelum melakukan klik Button POST, gunakan bootstrap valid-state agar fieldnya terlihat sukses atau tidak
6. Jika login benar dan sukses simpan response ke Store Vuex dan redirect ke halaman utama
7. Jika login gagal, tampilkan modal (popup) pesan error gagal dari response api.

## Pengiriman Hasil:
1. Push ke repository pribadi github anda secara public dengan judul bebas dan sopan
2. Auto deploy project anda ke netlify.com supaya karya anda terpublikasi online
3. Kirimkan link repo github dan link URL deploy (netlify) di atas ke

    Email: ```mail.firmansyah93@gmail.com```
    Subject: ```Mini Frontend Project PT Quantum Leap 2021```
4. Maksimum Batas Waktu Pengiriman yakni 3 hari setelah informasi testing kepada anda (Semakin cepat semakin baik)
